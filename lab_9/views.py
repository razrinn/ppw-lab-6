from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
import requests

# Create your views here.

response = {}


def index(request):
    if request.user.is_authenticated and "like" not in request.session:
        request.session["fullname"] = request.user.first_name + \
            " " + request.user.last_name
        request.session["username"] = request.user.username
        request.session["email"] = request.user.email
        request.session["sessionid"] = request.session.session_key
        request.session["like"] = []
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    return render(request, 'book.html')


def login(request):
    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('lab_9:index')


def getJSON(request, search='quilting'):
    json_data = requests.get(
        'https://www.googleapis.com/books/v1/volumes?q=' + search).json()
    arr_json = []
    for info in json_data['items']:
        data = {}
        data['title'] = info['volumeInfo']['title']
        if 'imageLinks' in info['volumeInfo']:
            data['thumbnail'] = info['volumeInfo']['imageLinks']['thumbnail']
        else:
            data['thumbnail'] = 'https://png.icons8.com/dotty/2x/nothing-found.png'
        if 'authors' in info['volumeInfo']:
            data['author'] = ", ".join(info['volumeInfo']['authors'])
        else:
            data['author'] = "Data tidak ditemukan"
        if 'publishedDate' in info['volumeInfo']:
            data['publishedDate'] = info['volumeInfo']['publishedDate']
        else:
            data['publishedDate'] = "Data tidak ditemukan"
        data['id'] = info['id']
        arr_json.append(data)

    response = {'data': arr_json}
    return JsonResponse(response)


@csrf_exempt
def like(request):
    if(request.method == "POST"):
        lst = request.session["like"]
        if request.POST["id"] not in lst:
            lst.append(request.POST["id"])
        request.session["like"] = lst
        response["message"] = len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")


@csrf_exempt
def unlike(request):
    if(request.method == "POST"):
        lst = request.session["like"]
        if request.POST["id"] in lst:
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"] = lst
        response["message"] = len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")


def get_like(request):
    if request.user.is_authenticated:
        if(request.method == "GET"):
            if request.session["like"] is not None:
                response["message"] = request.session["like"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)

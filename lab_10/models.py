from django.db import models

# Create your models here.


class Subscriber(models.Model):
    email = models.EmailField(max_length=32, unique=True)
    name = models.CharField(max_length=32)
    password = models.CharField(max_length=32)

    def __str__(self):
        return self.email

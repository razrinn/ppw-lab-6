from django import forms
from .models import Subscriber


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['email', 'name', 'password']
        widgets = {
            'email': forms.EmailInput(attrs={'type': 'email', 'placeholder': 'example@gmail.com'}),
            'name': forms.TextInput(attrs={'placeholder': 'My Full Name'}),
            'password': forms.PasswordInput(attrs={'type': 'password'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
